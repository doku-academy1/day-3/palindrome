import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
       String word = "";
       String reverseWord = "";
       char c;

        Scanner keyboard = new Scanner(System.in);
        System.out.print("Masukkan kata: ");
        word = keyboard.nextLine();


        for (int i = word.length()-1; i>=0 ; i--) {
            c = word.charAt(i);
            reverseWord += c;
        }

        if (reverseWord.equals(word)) {
            System.out.println("Palindrome");
        } else {
            System.out.println("Bukan Palindrome");
        }
    }
}